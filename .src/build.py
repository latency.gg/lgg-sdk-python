"""
Reads release.yml -> Pulls OpenAPI specs and docs from respective repos -> creates Merge Request
"""
import os
import shutil
from datetime import datetime

import click
import yaml
import jinja2
from git import Repo, RemoteReference


@click.command()
@click.option(
    "--tag",
    envvar="CI_COMMIT_TAG",
    prompt="Git Tag",
    help="The release tag",
    prompt_required=False,
)
def main(tag=None):
    root = os.getcwd().strip(".src")
    repo = Repo(root)
    if tag is None:
        tag = f"auto-{int(datetime.now().timestamp())}"
    
    with open(os.path.join(root,"release.yml")) as f:
        release = yaml.load(f, Loader=yaml.CLoader)
    
    path = os.path.join(root, "tmp")
    if os.path.isdir(path):
        shutil.rmtree(path)
    
    output = os.path.join(root, "output")
    if os.path.isdir(output):
        shutil.rmtree(output)
    os.mkdir(output)
        
    openapi = os.path.join(output, "openapi")
    if os.path.isdir(openapi):
        shutil.rmtree(openapi)
    os.mkdir(openapi)
    
    docs = os.path.join(output, "docs")
    if os.path.isdir(docs):
        shutil.rmtree(docs)
    os.mkdir(docs)
    
    clone(path, "probe_api_spec", release["probe"]["api"])
    if os.path.isfile(os.path.join(openapi, "probe.yaml")):
        os.remove(os.path.join(openapi, "probe.yaml"))
    shutil.copyfile(os.path.join(path, "probe_api_spec", "src", "openapi.yaml"), os.path.join(openapi, "probe.yaml"))
    
    clone(path, "client_api_spec", release["client"]["api"])
    if os.path.isfile(os.path.join(openapi, "client.yaml")):
        os.remove(os.path.join(openapi, "client.yaml"))
    shutil.copyfile(os.path.join(path, "client_api_spec", "src", "openapi.yaml"), os.path.join(openapi, "client.yaml"))
    
    clone(path, "common_docs", release["docs"]["common"])
    file_list = template_docs(
        os.path.join(path, "common_docs", "src"), 
        os.path.join(docs, "common"), 
        {
            "version": tag, 
            "language": f"{release['language']}-{release['language_version']}",
            "sdk_url": release["repo"],
        }
    )
    
    shutil.rmtree(path)
    clone(output, "client", release["client"]["example"])
    clone(output, "probe", release["probe"]["example"])

    
def clone(path, component_name, config):
    Repo.clone_from(config["repo"], os.path.join(path, component_name), branch=config["tag"], depth=1)


def template_docs(src, dest, values):
    if os.path.isdir(dest):
        shutil.rmtree(dest)
    os.mkdir(dest)
    loader = jinja2.FileSystemLoader(src)
    env = jinja2.Environment(loader=loader)
    file_list = []
    for filename in os.listdir(src):
        if not filename.endswith(".jinja2"):
            continue
        output_filename = os.path.join(dest, filename.strip(".jinja2"))
        with open(output_filename, "w") as output:
            output.write(env.get_template(filename).render(values))
        file_list.append(output_filename)
    return file_list
